/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>  // std::size_t
#include <memory>
#include <string>
#include <utility>  // std::pair
#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "aidge/operator/Pop.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Testing.hpp"
#include "aidge/recipes/Recipes.hpp"

using namespace Aidge;

TEST_CASE("[core/operators] MetaOperator", "[Operator][MetaOperator]") {
    SECTION("PaddedConv") {
        auto op = PaddedConv(1, 3, {3, 3}, "padded_conv", {1, 1}, {1, 1, 1, 1});

        auto microGraph = std::dynamic_pointer_cast<MetaOperator_Op>(op->getOperator())->getMicroGraph();

        REQUIRE(microGraph->getNodes().size() == 2);
        REQUIRE(microGraph->inputNodes().size() == 2);  // 2 because Conv has inputs outside the meta-op (Producers for weight and bias)
        REQUIRE(nodePtrTo(microGraph->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"Pad", 0}, {"Conv", 1}, {"Conv", 2}}));
        REQUIRE(nodePtrTo(microGraph->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"Conv", 0}}));
        REQUIRE(microGraph->outputNodes().size() == 1);
        REQUIRE((*microGraph->outputNodes().begin())->getOperator()->type() == "Conv");
        REQUIRE(op->nbInputs() == 3);
        REQUIRE(op->nbData() == 1);
        REQUIRE(op->nbOutputs() == 1);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(std::vector<std::size_t>({2,1,5,5}));
        std::shared_ptr<OperatorTensor> opTensor = std::static_pointer_cast<OperatorTensor>(op->getOperator());
        opTensor->associateInput(0,myInput);
        opTensor->forwardDims();

        REQUIRE(opTensor->dimsForwarded());
        REQUIRE(std::static_pointer_cast<Tensor>(opTensor->getRawOutput(0))->dims() == std::vector<size_t>({2,3,5,5}));
        REQUIRE(std::static_pointer_cast<Tensor>(opTensor->getRawInput(0)) == myInput);
        REQUIRE(microGraph->getOrderedInputs()[0].first->getOperator()->getRawInput(0) == myInput);
        REQUIRE(opTensor->getRawOutput(0) == (*microGraph->outputNodes().begin())->getOperator()->getRawOutput(0));

        //op->getOperator()->updateConsummerProducer();  // require implementation
        //auto microGraphScheduler = std::dynamic_pointer_cast<MetaOperator_Op>(op->getOperator())->getMicroGraphScheduler();
        //REQUIRE(microGraphScheduler->getStaticScheduling().size() == 2);
    }

    SECTION("LSTM") {
        auto myLSTM = LSTM(32, 64, 16, true, "ltsm");
        auto op = std::static_pointer_cast<OperatorTensor>(myLSTM->getOperator());

        auto microGraph = std::dynamic_pointer_cast<MetaOperator_Op>(op)->getMicroGraph();
        microGraph->save("lstm", false, false);

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->nbData() == 1);
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>();
        myInput->resize({32});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>();
        myInit->resize({1, 64});

        op->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        op->forwardDims();
        microGraph->save("lstm_dims", true, true);
        REQUIRE(op->dimsForwarded());

        //op->updateConsummerProducer();  // require implementation
        //auto microGraphScheduler = std::dynamic_pointer_cast<MetaOperator_Op>(op)->getMicroGraphScheduler();
        //microGraphScheduler->saveSchedulingDiagram("lstm_scheduling");
    }

    SECTION("LSTM(expanded)") {
        auto pop = Pop();
        auto myLSTM = LSTM(2, 3, 2, true, "ltsm");
        auto myGraph = Sequential({pop, myLSTM});
        auto op = std::static_pointer_cast<OperatorTensor>(myLSTM->getOperator());

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->nbData() == 1);
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array3D<float, 2, 3, 2>{{{{1.0, 2.0}, {3.0, 4.0}, {5.0, 6.0}}, {{2.0, 3.0}, {4.0, 5.0}, {6.0, 7.0}}}});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{0.1, 0.1}, {0.1, 0.1}, {0.1, 0.1}}});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}}});

        pop->getOperator()->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        myLSTM->input(1).first->getOperator()->setOutput(0, myInitW);
        op->setInput(2, myInitW);
        op->setInput(3, myInitW);
        op->setInput(4, myInitW);
        // Weights H
        op->setInput(5, myInitR);
        op->setInput(6, myInitR);
        op->setInput(7, myInitR);
        op->setInput(8, myInitR);

        auto g = getConnectedGraphView(myLSTM);
        g->save("lstm_before_expand", true, true);

        expandMetaOps(g);
        g->setRootNode(pop);
        REQUIRE(g->rootNode() == pop);
        g->save("lstm_expanded", true, true);

        REQUIRE(g->getNodes().size() == 41);
    }
}
