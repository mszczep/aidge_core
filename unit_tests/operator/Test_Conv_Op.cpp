/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[core/operator] Conv_Op(computeReceptiveField)", "[Operator][computeReceptiveField][Conv]") {
    auto dataProvider = Producer({16, 3, 224, 224}, "dataProvider");
    auto conv1 = Conv(3, 32, {5, 5}, "conv1");          // output dims: {16, 32, 220, 220}
    auto conv2 = Conv(32, 64, {3, 3}, "conv2");         // output dims: {16, 64, 218, 218}
    auto conv3 = Conv(64, 10, {2, 2}, "conv3", {2,2});  // output dims: {16, 10, 109, 109}
    auto conv4 = Conv(10, 10, {1, 1}, "conv4");         // output dims: {16, 10, 109, 109}

    auto g = std::make_shared<GraphView>("TestGraph");

    dataProvider->addChild(conv1, 0);
    g->add(conv1);
    g->addChild(conv2, conv1, 0);
    g->addChild(conv3, conv2, 0);
    g->addChild(conv4, conv3, 0);

    g->forwardDims();

    auto op1 = std::dynamic_pointer_cast<OperatorTensor>(conv1 -> getOperator());
    auto op2 = std::dynamic_pointer_cast<OperatorTensor>(conv2 -> getOperator());
    auto op3 = std::dynamic_pointer_cast<OperatorTensor>(conv3 -> getOperator());
    auto op4 = std::dynamic_pointer_cast<OperatorTensor>(conv4 -> getOperator());

    SECTION("Check individual receptive fields") {
        auto res1 = op1 -> computeReceptiveField({0,0,0,0}, {16,32,10,10});
        auto res2 = op2 -> computeReceptiveField({3,20,100,28}, {4,20,30,40});
        auto res3 = op3 -> computeReceptiveField({0,0,0,0}, {1,1,109,109});
        auto res4 = op4 -> computeReceptiveField({5,0,108,108}, {10,10,1,1});

        REQUIRE(((res1[0].first == std::vector<DimSize_t>({0,0,0,0})) && (res1[0].second == std::vector<DimSize_t>({16, 3, 14, 14}))));
        REQUIRE(((res1[1].first == std::vector<DimSize_t>({0,0,0,0})) && (res1[1].second == std::vector<DimSize_t>({32, 3, 5, 5}))));
        REQUIRE(((res1[2].first == std::vector<DimSize_t>({0})) && (res1[2].second == std::vector<DimSize_t>({32}))));
        REQUIRE(((res2[0].first == std::vector<DimSize_t>({3,0,100,28})) && (res2[0].second == std::vector<DimSize_t>({4, 32, 32, 42}))));
        REQUIRE(((res3[0].first == std::vector<DimSize_t>({0,0,0,0})) && (res3[0].second == std::vector<DimSize_t>({1, 64, 218, 218}))));
        REQUIRE(((res4[0].first == std::vector<DimSize_t>({5, 0, 108, 108})) && (res4[0].second == std::vector<DimSize_t>({10, 10, 1, 1}))));
    }

    SECTION("Check receptive field propagation") {
        // input:  first-{5, 0, 50, 50}  dims-{1, 1, 1, 1}
        auto res4 = op4->computeReceptiveField({5,0,50,50}, {1,1,1,1});
        // conv4 RF:  first-{5, 0, 50, 50}  dims-{1, 10, 1, 1}
        auto res3 = op3->computeReceptiveField(res4[0].first, res4[0].second);
        // conv3 RF:  first-{5, 0, 100, 100} dims-{1, 64, 2, 2}
        auto res2 = op2->computeReceptiveField(res3[0].first, res3[0].second);
        // conv2 RF:  first-{5, 0, 100, 100} dims-{1, 32, 4, 4}
        auto res1 = op1->computeReceptiveField(res2[0].first, res2[0].second);
        // conv1 RF:  first-{5, 0, 100, 100} dims-{1, 3, 8, 8}

        REQUIRE(((res1[0].first == std::vector<DimSize_t>({5, 0, 100, 100})) && (res1[0].second == std::vector<DimSize_t>({1, 3, 8, 8}))));


        // std::cout << "conv1: {";
        // std::cout << op1->input(0).getCoord(res1[0].first)[0] << ", "
        //           << op1->input(0).getCoord(res1[0].first)[1] << ", "
        //           << op1->input(0).getCoord(res1[0].first)[2] << ", "
        //           << op1->input(0).getCoord(res1[0].first)[3] << "} - {";
        // std::cout << res1[0].second[0] << ", "
        //           << res1[0].second[1] << ", "
        //           << res1[0].second[2] << ", "
        //           << res1[0].second[3] << "}" << std::endl;
    }
}
}  // namespace Aidge