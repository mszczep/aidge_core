

/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/operator/Producer.hpp"


using namespace Aidge;
TEST_CASE("get Delta") {


        std::shared_ptr<GraphView> g1 = std::make_shared<GraphView>("TestGraph");
        std::shared_ptr<Node> conv = GenericOperator("Conv", 1, 0, 1, "c");
        std::shared_ptr<Node> conv1 = GenericOperator("Conv", 1, 0, 1, "c1");
        std::shared_ptr<Node> conv2 = GenericOperator("Conv", 1, 0, 1, "c2");
        std::shared_ptr<Node> conv3 = GenericOperator("Conv", 1, 0, 1, "c3");
        std::shared_ptr<Node> conv3_5 = GenericOperator("Conv", 1, 0, 1, "c3.5");
        std::shared_ptr<Node> conv4 = GenericOperator("Conv", 1, 0, 1, "c4");
        std::shared_ptr<Node> conv5 = GenericOperator("Conv", 1, 0, 1, "c5");

        g1->add(conv);
        g1->addChild(conv1, "c");


        std::set<Aidge::NodePtr> see;
conv->getNodeDelta(1,see);

        SECTION("Self return") {
            see.clear();
             REQUIRE(conv->getNodeDelta(0,see) == std::set<std::shared_ptr<Node>>{conv});
        }


        SECTION("child") {
             see.clear();
             REQUIRE(conv->getNodeDelta(1,see) == std::set<std::shared_ptr<Node>>{conv1});
        }


}