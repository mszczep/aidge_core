
#include <catch2/catch_test_macros.hpp>
#include "aidge/nodeTester/ConditionalInterpreter.hpp"
#include "aidge/operator/GenericOperator.hpp"


using namespace Aidge;



TEST_CASE("ConditionalInterpreter", "ConditionalInterpreter") {

    SECTION("custom Lambda") {

        
        ConditionalInterpreter conditionalParserB = ConditionalInterpreter("A"," bad($) == false ");
        ConditionalInterpreter conditionalParserG = ConditionalInterpreter("A"," good($) == true ");


        conditionalParserB.insertLambda("bad",+[](NodePtr NodeOp){return NodeOp->name() == "ZZ";});
        conditionalParserG.insertLambda("good",+[](NodePtr NodeOp){return NodeOp->name() == "Gop1";});
        std::shared_ptr<Node> nodeOp = GenericOperator("conv", 0, 0, 0, "Gop1");

        REQUIRE(conditionalParserB.test(nodeOp) == true);
        REQUIRE(conditionalParserG.test(nodeOp) == true);
    }
    

     ConditionalInterpreter conditionalParserT = ConditionalInterpreter("A","isConv($)==true");
     conditionalParserT.insertLambda("isConv",+[](NodePtr NodeOp){return NodeOp->type() == "Conv";});
     std::shared_ptr<Node> zz = GenericOperator("conv", 0, 0, 0, "Gop1");
     conditionalParserT.test(zz);

    SECTION("Lambdas") {
        ConditionalInterpreter conditionalParser = ConditionalInterpreter("OP_test","getType($) =='Conv' || getType($) =='FC' ");

        std::shared_ptr<Node> A = GenericOperator("Conv", 0, 0, 0, "A");
        REQUIRE(conditionalParser.test(A) == true);

        std::shared_ptr<Node> B = GenericOperator("FC", 0, 0, 0, "B");
        REQUIRE(conditionalParser.test(B) == true);


        std::shared_ptr<Node> C = GenericOperator("A", 0, 0, 0, "C");
        conditionalParser.test(C);
        REQUIRE(conditionalParser.test(C) == false);
    }

    SECTION("syntax error") {

        const std::string test = "'A' == 'A' ,&& ";
        REQUIRE_THROWS_AS( ConditionalInterpreter("A",test), std::runtime_error);
  
    }


    SECTION("test false int ") {

        const std::string test = " 10 == 11 " ;
        ConditionalInterpreter conditionalParser = ConditionalInterpreter("A",test);
        std::shared_ptr<Node> nodeOp = GenericOperator("conv", 0, 0, 0, "Gop1");
        bool result = conditionalParser.test(nodeOp);
        REQUIRE(result == false);
    }

    SECTION("test true int ") {
        const std::string test = " 42 == 42 " ;
        ConditionalInterpreter conditionalParser = ConditionalInterpreter("A",test);
        std::shared_ptr<Node> nodeOp = GenericOperator("conv", 0, 0, 0, "Gop1");
        bool result = conditionalParser.test(nodeOp);
        REQUIRE(result == true);
    }
    
    SECTION("test false str ") {
        const std::string test = " 'toto' == 'Corgi' " ;
        ConditionalInterpreter conditionalParser = ConditionalInterpreter("A",test);
        std::shared_ptr<Node> nodeOp = GenericOperator("conv", 0, 0, 0, "Gop1");
        bool result = conditionalParser.test(nodeOp);
        REQUIRE(result == false);
    }

    SECTION("test true str ") {

        const std::string test = " 'Corgi' == 'Corgi' " ;
        ConditionalInterpreter conditionalParser = ConditionalInterpreter("A",test);
        std::shared_ptr<Node> nodeOp = GenericOperator("conv", 0, 0, 0, "Gop1");
        bool result = conditionalParser.test(nodeOp);
        REQUIRE(result == true);
    }

}