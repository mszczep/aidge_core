
#include <catch2/catch_test_macros.hpp>
#include "aidge/nodeTester/ConditionalParser.hpp"
#include "aidge/utilsParsing/AstNode.hpp"

using namespace Aidge;

    std::string gVal() {
        int randomValue = std::rand() % 5;
        switch (randomValue) {
        case 0:
            return std::to_string(std::rand() % 101);

        case 1:
            return std::to_string(std::rand() % 101)+"."+std::to_string(std::rand() % 101);

        case 2:
             return " 'toto' ";
        case 3:
             return " A ";

        case 4:
             return " A(10) ";

        default:
             return " true ";

        }
    }

    std::string gExpr() ;
    std::string gCmpr() {
        int randomValue = std::rand() % 3;
        switch (randomValue) {
            case 0:
                return  gVal() + " == " +gVal();
            case 1:
                return  "("+ gExpr() +")";
            default:
                return  gVal() + " != " +gVal();

        }


        return gVal() + " == " +gVal();
    }

    std::string gExpr() {
        std::string out = gCmpr();
        int iterations = std::rand() % 100;
        for (int i = 0; i < iterations; ++i) {
            int randomValue = std::rand() % 2;
            switch (randomValue) {
                case 0:
                    return  out +" && " + gCmpr();
                    break;
                default:
                    return  out +" || " + gCmpr();
                    break;
            }
        }
        return out;
    }


TEST_CASE("ConditionalParser", "ConditionalParser") {

    SECTION("Empty") {
        for (int i = 0; i < 100; ++i) {
            const std::string test = gExpr();
            ConditionalParser conditionalParser = ConditionalParser(test);
            std::shared_ptr<AstNode<ConditionalTokenTypes>> tree = conditionalParser.parse();
        }
    }
}