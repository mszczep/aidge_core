/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/recipes/LabelGraph.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/graph/OpArgs.hpp"
#include <cstddef>

using namespace Aidge;

TEST_CASE("[LabelGraph] conv") {
    auto dataProvider = Producer({16, 3, 224, 224}, "dataProvider");
    auto conv1 = Conv(3, 32, {3, 3}, "conv1");
    auto conv2 = Conv(32, 64, {3, 3}, "conv2");
    auto conv3 = Conv(64, 10, {1, 1}, "conv3");
    auto g1 = std::make_shared<GraphView>("TestGraph");
    dataProvider->addChild(conv1, 0);
    g1->add(conv1);
    g1->addChild(conv2, conv1, 0);
    g1->addChild(conv3, conv2, 0);
    g1->save("LabelGraph_conv_graph");

    auto g2 = labelGraph(g1);

    auto dataProvider2 = Producer({16, 3, 224, 224}, "dataProvider");
    dataProvider2->addChild(g2->getNode("conv1"), 0);

    g2->forwardDims();
    g2->save("LabelGraph_conv_label");

    SECTION("Check resulting nodes") {
        REQUIRE(g2->getNodes().size() == 3);
        REQUIRE(g2->getNode("conv1")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv1")->getOperator()->getRawOutput(0) == g2->getNode("conv2")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv2")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv2")->getOperator()->getRawOutput(0) == g2->getNode("conv3")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv3")->getOperator()->type() == "MaxPooling");
    }
}

TEST_CASE("[LabelGraph] deleted node") {
    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        Conv(3, 32, {3, 3}, "conv1"),
        GenericOperator("Dummy_to_be_removed", 1, 0, 1),
        Conv(32, 64, {3, 3}, "conv2"),
        Conv(64, 10, {1, 1}, "conv3", {2, 2})
    });

    g1->save("LabelGraph_deleted_graph");

    auto g2 = labelGraph(g1);

    auto dataProvider2 = Producer({16, 1, 224, 224}, "dataProvider");
    dataProvider2->addChild(g2->getNode("conv1"), 0);

    g2->forwardDims();
    g2->save("LabelGraph_deleted_label");

    SECTION("Check resulting nodes") {
        REQUIRE(g2->getNodes().size() == 3);
        REQUIRE(g2->getNode("conv1")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv1")->getOperator()->getRawOutput(0) == g2->getNode("conv2")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv2")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv2")->getOperator()->getRawOutput(0) == g2->getNode("conv3")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv3")->getOperator()->type() == "MaxPooling");
    }

    SECTION("Check dimensions") {
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("conv1")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 222, 222}));
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("conv2")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 220, 220}));
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("conv3")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 110, 110}));
    }
}

TEST_CASE("[LabelGraph] deleted nodes") {
    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        Conv(3, 32, {3, 3}, "conv1"),
        GenericOperator("Dummy_to_be_removed", 1, 0, 1),
        GenericOperator("Dummy_to_be_removed", 1, 0, 1),
        GenericOperator("Dummy_to_be_removed", 1, 0, 1),
        Conv(32, 64, {3, 3}, "conv2"),
        GenericOperator("Dummy_to_be_removed", 1, 0, 1),
        Conv(64, 10, {1, 1}, "conv3")
    });

    g1->save("LabelGraph_deleteds_graph");

    auto g2 = labelGraph(g1);

    auto dataProvider2 = Producer({16, 3, 224, 224}, "dataProvider");
    dataProvider2->addChild(g2->getNode("conv1"), 0);

    g2->forwardDims();
    g2->save("LabelGraph_deleteds_label");

    SECTION("Check resulting nodes") {
        REQUIRE(g2->getNodes().size() == 3);
        REQUIRE(g2->getNode("conv1")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv1")->getOperator()->getRawOutput(0) == g2->getNode("conv2")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv2")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("conv2")->getOperator()->getRawOutput(0) == g2->getNode("conv3")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("conv3")->getOperator()->type() == "MaxPooling");
    }
}

TEST_CASE("[LabelGraph] pooling") {
    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        AvgPooling({2, 2}, "pool1"),
        MaxPooling({2, 2}, "pool2"),
        MaxPooling({2, 2}, "pool3", {2, 2})
    });

    g1->save("LabelGraph_deleted_graph");

    auto g2 = labelGraph(g1);

    auto dataProvider2 = Producer({16, 1, 224, 224}, "dataProvider");
    dataProvider2->addChild(g2->getNode("pool1"), 0);

    g2->forwardDims();
    g2->save("LabelGraph_pooling");

    SECTION("Check resulting nodes") {
        REQUIRE(g2->getNodes().size() == 3);
        REQUIRE(g2->getNode("pool1")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("pool1")->getOperator()->getRawOutput(0) == g2->getNode("pool2")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("pool2")->getOperator()->type() == "MaxPooling");
        REQUIRE(g2->getNode("pool2")->getOperator()->getRawOutput(0) == g2->getNode("pool3")->getOperator()->getRawInput(0));
        REQUIRE(g2->getNode("pool3")->getOperator()->type() == "MaxPooling");
    }

    SECTION("Check dimensions") {
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("pool1")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 223, 223}));
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("pool2")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 222, 222}));
        REQUIRE(std::static_pointer_cast<Tensor>(g2->getNode("pool3")->getOperator()->getRawOutput(0))->dims() == std::vector<DimSize_t>({16, 1, 111, 111}));
    }
}
