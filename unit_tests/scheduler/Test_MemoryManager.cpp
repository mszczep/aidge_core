/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/operator/GenericOperator.hpp"
#include "aidge/scheduler/MemoryManager.hpp"

using namespace Aidge;

TEST_CASE("allocate1", "[MemoryManager]") {
    std::shared_ptr<Node> node1
        = GenericOperator("Fictive", 0, 0, 0, "node1");
    std::shared_ptr<Node> node2
        = GenericOperator("Fictive", 0, 0, 0, "node2");
    std::shared_ptr<Node> node3
        = GenericOperator("Fictive", 0, 0, 0, "node3");
    std::shared_ptr<Node> node4
        = GenericOperator("Fictive", 0, 0, 0, "node4");

    MemoryManager memManager;
    memManager.allocate(node1, 1024, {node2});

    REQUIRE(memManager.getPeakUsage() == 1024);
    REQUIRE(memManager.getPlanes(node1).size() == 1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node1).back().count == 1);
    REQUIRE(memManager.getPlanes(node1).back().length == 1);
    REQUIRE(memManager.getPlanes(node1).back().stride == 1024);

    memManager.releaseDependencies(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);

    memManager.tick();
    memManager.release(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == 1);

    memManager.allocate(node2, 2048, {node3});

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048);
    REQUIRE(memManager.getPlanes(node2).size() == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 1024);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);

    const std::vector<MemoryManager::MemoryPlane>& memPlanes
        = memManager.getPlanes(node2);

    REQUIRE(memPlanes.size() == 1);

    memManager.reallocate(memPlanes.back().memSpace,
                          node3, 512, 2048, false, 0, {node4});

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).size() == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->offset == 1024);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->size == 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).back().offset == 512);
    REQUIRE(memManager.getPlanes(node3).back().size == 2048);
    REQUIRE(memManager.getPlanes(node3).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node3).back().count == 1);
    REQUIRE(memManager.getPlanes(node3).back().length == 1);
    REQUIRE(memManager.getPlanes(node3).back().stride == 2048);

    memManager.releaseDependencies(node3);
    memManager.tick();
    memManager.release(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == 3);

    memManager.allocate(node4, 1024);

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node4).size() == 1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 3);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node4).back().offset == 0);
    REQUIRE(memManager.getPlanes(node4).back().size == 1024);
    REQUIRE(memManager.getPlanes(node4).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node4).back().count == 1);
    REQUIRE(memManager.getPlanes(node4).back().length == 1);
    REQUIRE(memManager.getPlanes(node4).back().stride == 1024);

    memManager.releaseDependencies(node4);
    memManager.tick();
    memManager.release(node4);

    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 3);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == 4);

    memManager.log("MemoryManager_allocate1.log");
}

TEST_CASE("allocate2", "[MemoryManager]") {
    std::shared_ptr<Node> node1
        = GenericOperator("Fictive", 0, 0, 0, "node1");
    std::shared_ptr<Node> node2
        = GenericOperator("Fictive", 0, 0, 0, "node2");
    std::shared_ptr<Node> node3
        = GenericOperator("Fictive", 0, 0, 0, "node3");
    std::shared_ptr<Node> node4
        = GenericOperator("Fictive", 0, 0, 0, "node4");

    MemoryManager memManager;
    memManager.allocate(node1, 1024, {node2});

    REQUIRE(memManager.getPeakUsage() == 1024);
    REQUIRE(memManager.getPlanes(node1).size() == 1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node1).back().count == 1);
    REQUIRE(memManager.getPlanes(node1).back().length == 1);
    REQUIRE(memManager.getPlanes(node1).back().stride == 1024);

    memManager.releaseDependencies(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);

    memManager.tick();
    memManager.release(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == 1);

    memManager.allocate(node2, 2048, {node3});

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048);
    REQUIRE(memManager.getPlanes(node2).size() == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 1024);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);

    const std::vector<MemoryManager::MemoryPlane>& memPlanes
        = memManager.getPlanes(node1);

    REQUIRE(memPlanes.size() == 1);

    memManager.reallocate(memPlanes.back().memSpace,
                          node3, 512, 2048, false, 0, {node4});

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).size() == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->size == 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node4}));
    REQUIRE(memManager.getPlanes(node3).back().offset == 512);
    REQUIRE(memManager.getPlanes(node3).back().size == 2048);
    REQUIRE(memManager.getPlanes(node3).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node3).back().count == 1);
    REQUIRE(memManager.getPlanes(node3).back().length == 1);
    REQUIRE(memManager.getPlanes(node3).back().stride == 2048);

    // node2 memSpace should have moved
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 2048 + 512);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node4}));
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == 3);

    memManager.allocate(node4, 1024);

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node4).size() == 1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 3);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->offset == 2048 + 512);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node4).back().offset == 0);
    REQUIRE(memManager.getPlanes(node4).back().size == 1024);
    REQUIRE(memManager.getPlanes(node4).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node4).back().count == 1);
    REQUIRE(memManager.getPlanes(node4).back().length == 1);
    REQUIRE(memManager.getPlanes(node4).back().stride == 1024);

    memManager.releaseDependencies(node4);
    memManager.tick();
    memManager.release(node4);

    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 3);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == 4);

    memManager.log("MemoryManager_allocate2.log");
}

TEST_CASE("allocate3", "[MemoryManager]") {
    std::shared_ptr<Node> node1
        = GenericOperator("Fictive", 0, 0, 0, "node1");
    std::shared_ptr<Node> node2
        = GenericOperator("Fictive", 0, 0, 0, "node2");
    std::shared_ptr<Node> node3
        = GenericOperator("Fictive", 0, 0, 0, "node3");
    std::shared_ptr<Node> node4
        = GenericOperator("Fictive", 0, 0, 0, "node4");

    MemoryManager memManager;
    memManager.allocate(node1, 1024, {node2});

    REQUIRE(memManager.getPeakUsage() == 1024);
    REQUIRE(memManager.getPlanes(node1).size() == 1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node1).back().count == 1);
    REQUIRE(memManager.getPlanes(node1).back().length == 1);
    REQUIRE(memManager.getPlanes(node1).back().stride == 1024);

    memManager.releaseDependencies(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);

    memManager.tick();
    memManager.release(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == 1);

    memManager.allocate(node2, 2048, {node3});

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048);
    REQUIRE(memManager.getPlanes(node2).size() == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 1024);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);

    const std::vector<MemoryManager::MemoryPlane>& memPlanes
        = memManager.getPlanes(node1);

    REQUIRE(memPlanes.size() == 1);

    memManager.reallocate(memPlanes.back().memSpace,
                          node3, 512, 2048, false);

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).size() == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->size == 2048 + 512);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node3).back().offset == 512);
    REQUIRE(memManager.getPlanes(node3).back().size == 2048);
    REQUIRE(memManager.getPlanes(node3).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node3).back().count == 1);
    REQUIRE(memManager.getPlanes(node3).back().length == 1);
    REQUIRE(memManager.getPlanes(node3).back().stride == 2048);

    // node2 memSpace should have moved
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 2048 + 512);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == 3);

    memManager.reallocate(memPlanes.back().memSpace,
                          node4, 256, 1024, false);

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048 + 512);
    REQUIRE(memManager.getPlanes(node4).size() == 1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->size == 2048 + 512);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node4).back().offset == 256);
    REQUIRE(memManager.getPlanes(node4).back().size == 1024);
    REQUIRE(memManager.getPlanes(node4).back().getLimit() == 2048 + 256);
    REQUIRE(memManager.getPlanes(node4).back().count == 1);
    REQUIRE(memManager.getPlanes(node4).back().length == 1);
    REQUIRE(memManager.getPlanes(node4).back().stride == 1024);

    // node2 memSpace should not have moved
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 2048 + 512);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node4);
    memManager.tick();
    memManager.release(node4);

    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == 4);

    memManager.log("MemoryManager_allocate3.log");
}

TEST_CASE("allocate3_wrapAround", "[MemoryManager]") {
    std::shared_ptr<Node> node1
        = GenericOperator("Fictive", 0, 0, 0, "node1");
    std::shared_ptr<Node> node2
        = GenericOperator("Fictive", 0, 0, 0, "node2");
    std::shared_ptr<Node> node3
        = GenericOperator("Fictive", 0, 0, 0, "node3");
    std::shared_ptr<Node> node4
        = GenericOperator("Fictive", 0, 0, 0, "node4");

    MemoryManager memManager;
    memManager.allocate(node1, 1024, {node2});

    REQUIRE(memManager.getPeakUsage() == 1024);
    REQUIRE(memManager.getPlanes(node1).size() == 1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().offset == 0);
    REQUIRE(memManager.getPlanes(node1).back().size == 1024);
    REQUIRE(memManager.getPlanes(node1).back().getLimit() == 1024);
    REQUIRE(memManager.getPlanes(node1).back().count == 1);
    REQUIRE(memManager.getPlanes(node1).back().length == 1);
    REQUIRE(memManager.getPlanes(node1).back().stride == 1024);

    memManager.releaseDependencies(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node2}));
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == -1);

    memManager.tick();
    memManager.release(node1);

    REQUIRE(memManager.getPlanes(node1).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->released == 1);

    memManager.allocate(node2, 2048, {node3});

    REQUIRE(memManager.getPeakUsage() == 1024 + 2048);
    REQUIRE(memManager.getPlanes(node2).size() == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 1024);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node1).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node2);

    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);

    const std::vector<MemoryManager::MemoryPlane>& memPlanes
        = memManager.getPlanes(node1);

    REQUIRE(memPlanes.size() == 1);

    memManager.reallocate(memPlanes.back().memSpace,
                          node3, 512, 2048, true);

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048);
    REQUIRE(memManager.getPlanes(node3).size() == 1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node3).back().offset == 512);
    REQUIRE(memManager.getPlanes(node3).back().size == 2048);
    REQUIRE(memManager.getPlanes(node3).back().getLimit() == 2048 - 512);
    REQUIRE(memManager.getPlanes(node3).back().count == 1);
    REQUIRE(memManager.getPlanes(node3).back().length == 1);
    REQUIRE(memManager.getPlanes(node3).back().stride == 2048);

    // node2 memSpace should have moved
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies ==
        std::set<std::shared_ptr<Node> >({node3}));
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->dependencies.empty());

    memManager.tick();
    memManager.release(node3);

    REQUIRE(memManager.getPlanes(node3).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node3).back().memSpace->released == 3);

    memManager.reallocate(memPlanes.back().memSpace,
                          node4, 1024, 1792, true);

    REQUIRE(memManager.getPeakUsage() == 2048 + 2048);
    REQUIRE(memManager.getPlanes(node4).size() == 1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == -1);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->offset == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->dependencies.empty());
    REQUIRE(memManager.getPlanes(node4).back().offset == 1024);
    REQUIRE(memManager.getPlanes(node4).back().size == 1792);
    REQUIRE(memManager.getPlanes(node4).back().getLimit() == 2048 - 1024);
    REQUIRE(memManager.getPlanes(node4).back().count == 1);
    REQUIRE(memManager.getPlanes(node4).back().length == 1);
    REQUIRE(memManager.getPlanes(node4).back().stride == 1792);

    // node2 memSpace should not have moved
    REQUIRE(memManager.getPlanes(node2).back().memSpace->allocated == 1);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->released == 2);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->offset == 2048);
    REQUIRE(memManager.getPlanes(node2).back().memSpace->size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().offset == 0);
    REQUIRE(memManager.getPlanes(node2).back().size == 2048);
    REQUIRE(memManager.getPlanes(node2).back().getLimit() == 2048);
    REQUIRE(memManager.getPlanes(node2).back().count == 1);
    REQUIRE(memManager.getPlanes(node2).back().length == 1);
    REQUIRE(memManager.getPlanes(node2).back().stride == 2048);

    memManager.releaseDependencies(node4);
    memManager.tick();
    memManager.release(node4);

    REQUIRE(memManager.getPlanes(node4).back().memSpace->allocated == 0);
    REQUIRE(memManager.getPlanes(node4).back().memSpace->released == 4);

    memManager.log("MemoryManager_allocate3_wrapAround.log");
}
