#include <catch2/catch_test_macros.hpp>
#include "aidge/graphRegex/GraphLexer.hpp"
#include "aidge/graphRegex/GraphRegexTypes.hpp"

#include "aidge/utilsParsing/ParsingToken.hpp"


#include <iostream>
#include <map>
#include <functional>

using namespace Aidge;

// NEXT
// QOM
// QZM
// KEY
// CKEY
// SEP
// LPAREN
// RPAREN

TEST_CASE("GraphRegex", "Lexer") {
    SECTION("RandomGenerateTest") {

        std::map<gRegexTokenTypes, std::function<std::pair<std::string, std::string>()>> LexerTestMap{
            {gRegexTokenTypes::NEXT,      +[](){return std::pair<std::string, std::string>("-> ","");}},
            {gRegexTokenTypes::QOM,       +[](){return std::pair<std::string, std::string>("+ ","");}},
            {gRegexTokenTypes::QZM,       +[](){return std::pair<std::string, std::string>("* ","");}},
            {gRegexTokenTypes::SEP,      +[](){return std::pair<std::string, std::string>("; ","");}},



            {gRegexTokenTypes::KEY,   +[](){
                std::size_t keyLen = (std::rand() % 20)+1;
                const std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_";
                std::size_t randomIndex = std::rand() % characters.size();
                std::string key;
                for (std::size_t i = 0; i < keyLen; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }

                return std::pair<std::string, std::string>(key+" ",key);}
            },

           {gRegexTokenTypes::CKEY,   +[](){
                std::size_t keyLen = (std::rand() % 20)+1;
                const std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_";
                const std::string num = "1234567890";

                std::size_t randomIndex = std::rand() % characters.size();
                std::size_t randomNum = std::rand() % num.size();
                std::string key;
                std::string idx;

                for (std::size_t i = 0; i < keyLen; ++i) {
                    key += characters[randomIndex];
                    idx += num[randomNum];
                    randomIndex = std::rand() % characters.size();
                    randomNum = std::rand() % num.size();
                }

                return std::pair<std::string, std::string>(key+"#"+idx+" ",key+"#"+idx);}
            },

            {gRegexTokenTypes::LPAREN,   +[](){return std::pair<std::string, std::string>("( ","");}},
            {gRegexTokenTypes::RPAREN,   +[](){return std::pair<std::string, std::string>(") ","");}}
            //{gRegexTokenTypes::STOP,     +[](){return std::pair<std::string, std::string>("","");}}
        };


        //////////////////
        //TEST GENERATOR
        //////////////////
        const std::size_t numRandomElements = 10000;
        std::vector<std::tuple<gRegexTokenTypes, std::string>> testVector;

        std::string testString;

        for (std::size_t i = 0; i < numRandomElements; ++i) {

            int randomIndex = std::rand() % LexerTestMap.size();
            // Get an iterator to the random element in the map
            auto it = std::next(LexerTestMap.begin(), randomIndex);
            // Access the random key and lambda value separately using structured binding
            gRegexTokenTypes randomKey = it->first;

            std::function<std::pair<std::string, std::string>()> randomValue = it->second;
            std::pair<std::string, std::string> result = randomValue();
        
            testString += result.first;
            testVector.emplace_back(randomKey, result.second);

       
        }

        GraphLexer graphLexer = GraphLexer(testString);

        for (std::tuple<gRegexTokenTypes, std::string> testToken : testVector) {
            gRegexTokenTypes tokenToFind = std::get<0>(testToken);
            std::string lexemToFind = std::get<1>(testToken);
            std::shared_ptr<ParsingToken<gRegexTokenTypes>> token = graphLexer.getNextToken();


            std::ostringstream errorMessage;
            errorMessage << "\n we whant :"<< lexemToFind << "\n we get : "<< token->getLexeme() <<"\n"<< "on \n" << testString << " :\n "  ;

            CAPTURE(errorMessage.str());
            REQUIRE(token->getLexeme() == lexemToFind);
            REQUIRE(token->getType() == tokenToFind);
        }
        std::shared_ptr<ParsingToken<gRegexTokenTypes>> token = graphLexer.getNextToken();
        REQUIRE(token->getType() == gRegexTokenTypes::STOP);
    }


}