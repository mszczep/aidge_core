/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Scaling.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;

namespace Aidge {

void init_Scaling(py::module& m) 
{
    py::class_<Scaling_Op, std::shared_ptr<Scaling_Op>, Attributes, OperatorTensor>(m, "ScalingOp", py::multiple_inheritance())
    .def("get_inputs_name", &Scaling_Op::getInputsName)
    .def("get_outputs_name", &Scaling_Op::getOutputsName)
    .def("attributes_name", &Scaling_Op::staticGetAttrsName);
    declare_registrable<Scaling_Op>(m, "ScalingOp");
    m.def("Scaling", &Scaling, py::arg("scaling_factor") = 1.0f, py::arg("nb_bits") = 8, py::arg("is_output_unsigned") = true, py::arg("name") = "");
}

}  // namespace Aidge
