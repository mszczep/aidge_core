#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/data/Database.hpp"
#include "aidge/data/Tensor.hpp"

namespace py = pybind11;
namespace Aidge {

/**
 * @brief Trampoline class for binding
 *
 */
class pyDatabase : public Database {
   public:
    using Database::Database;  // Inherit constructors

    std::vector<std::shared_ptr<Tensor>> getItem(
        const std::size_t index) const override {
        PYBIND11_OVERRIDE_PURE_NAME(std::vector<std::shared_ptr<Tensor>>, Database,
                               "get_item", getItem, index);
    }
    std::size_t getLen() const noexcept override {
        PYBIND11_OVERRIDE_PURE_NAME(std::size_t, Database, "len", getLen);
    }
    std::size_t getNbModalities() const noexcept override {
        PYBIND11_OVERRIDE_PURE_NAME(std::size_t, Database, "get_nb_modalities",
                               getNbModalities);
    }
};

void init_Database(py::module& m) {
    py::class_<Database, std::shared_ptr<Database>, pyDatabase>(
        m, "Database", py::dynamic_attr())
        .def(py::init<>())
        .def("get_item", &Database::getItem)
        .def("len", &Database::getLen)
        .def("get_nb_modalities", &Database::getNbModalities);
}
}  // namespace Aidge
