/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Cast.hpp"

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

void Aidge::Cast_OpImpl::forward() {
    const Cast_Op& op = dynamic_cast<const Cast_Op&>(mOp);
    op.getOutput(0)->copyCast(*(op.getInput(0)));
}

const std::string Aidge::Cast_Op::Type = "Cast";

void Aidge::Cast_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Cast_Op>::exists({name})) {
        SET_IMPL_MACRO(Cast_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Cast_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}
