/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <string>

#include "aidge/operator/Identity.hpp"

const std::string Aidge::Identity_Op::Type = "Identity";

void Aidge::Identity_Op::forward() {
    // Perform a shallow copy
    *(mOutputs[0]) = *(mInputs[0]);
    runHooks();
}
